let links = document.getElementsByTagName('A')
for(x = 0; x < links.length; x++) {
  if (typeof(links[x].href) === 'string') {
    let matches = links[x].href.match(/vid=(\d+)$/)
    if (matches) {
      links[x].onclick = function() {
        fetch(`/videos/view?vid=${matches[1]}`)
          .then(response => { return response.text() })
          .then(responseText => {
            let matches = responseText.match(/<video.+?src="(.+?)"/ms)
            if (matches) {
              chrome.runtime.sendMessage(
                { tsuPopup: { url: matches[1], title: responseText.match(/<title>(.+?)<\/title>/ims)[1] } },
                function (messageResponse) {
                  console.log(messageResponse.response)
                }
              )
            }
          })
          .catch(error => {})
        return false
      }
    }
  }
}

