chrome.runtime.onMessage.addListener(
  function(request, sender, sendResponse) {
    if (request.tsuPopup) {
      chrome.windows.create({
        url:    'data:text/html,' + getHTML(request.tsuPopup),
        type:   chrome.windows.WindowType.POPUP,
        width:  852,
        height: 480,
        left:   1000,
        top:    500
      })
      sendResponse({ response: 'done' });
    }
  }
);

function getHTML(tsuPopup) {

  return encodeURI(`
<html>
<head>
<meta charset="utf-8">
<title>${tsuPopup.title}</title>
<style>
html, body {
  padding: 0px;
  margin: 0px;
  overflow: hidden;
}
video {
  position: absolute;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
  outline: 0;
}
</style>
</head>
<body>
<video src="${tsuPopup.url}" autoplay controls></video>
</body>
</html>
`)

}
